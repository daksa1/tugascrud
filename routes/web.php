<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\KaryawanController::class,'index']);
Route::get('/adddata', [App\Http\Controllers\KaryawanController::class,'adddata']);
Route::post('/store', [App\Http\Controllers\KaryawanController::class,'store']);
Route::get('/edit{id}', [App\Http\Controllers\KaryawanController::class,'edit']);
Route::post('/update', [App\Http\Controllers\KaryawanController::class,'update']);
Route::get('/hapus{id}', [App\Http\Controllers\KaryawanController::class,'hapus']);
Auth::routes();
Route::get('/home', [App\Http\Controllers\KaryawanController::class,'index'])->name('home');






