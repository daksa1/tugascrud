<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tugas CRUD Laravel</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="asset/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/css/util.css">
	<link rel="stylesheet" type="text/css" href="asset/css/main.css">
    <link rel="stylesheet" type="text/css" href="asset/css/add.css">
<!--===============================================================================================-->
</head>
<body>

	
	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
					<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">Nomor Karyawan</th>
								<th class="column2">Nama Karyawan</th>
								<th class="column3">Nomor Telephone</th>
								<th class="column4">ID</th>
								<th class="column5">Jabatan</th>
								<th class="column6">Divisi</th>
                                <th class="column6">Perubahan</th>
							</tr>
						</thead>
                        @foreach ($karyawan as $p)
						<tbody>
							<tr>
                                <td>{{ $p->no_karyawan }}</td>
                                <td>{{ $p->nama_karyawan }}</td>
                                <td>{{ $p->no_telp_karyawan }}</td>
                                <td>{{ $p->id }}</td>
                                <td>{{ $p->jabatan_karyawan }}</td>
                                <td>{{ $p->divisi_karyawan }}</td>
                               <center>
                                <td>
                                    <button type="submit"><strong><a href="edit{{ $p->id }}">Edit</a></strong></button>
                                  	<button type="submit"><strong><a href="hapus{{ $p->id }}">Delete</a></strong></button>
                                </td>
                               </center>
                            </tr>
						</tbody>
                        @endforeach
					</table>
				</div>
				<center>
                <div class="button">
                    <button type="submit"><strong><a href="/adddata/">Tambah Data</a></strong></button>
                </div>
			</center>
			</div>
		</div>
	</div>


	

<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>