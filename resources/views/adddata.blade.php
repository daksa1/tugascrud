<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tugas CRUD Laravel</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="asset/images/icons/favicon.ico"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="asset/css/util.css">
	<link rel="stylesheet" type="text/css" href="asset/css/main.css">
    <link rel="stylesheet" type="text/css" href="asset/css/add.css">
<!--===============================================================================================-->
</head>
<body>

	
	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
                    <form action="store" method="POST">
                        @csrf
                        @method("POST")
                        <h1>Tambah Data Karyawan</h1>
                        <div class="formcontainer">
                        <div class="container">
                          <label for="uname"><strong>Nama Karyawan</strong></label>
                          <input type="text" placeholder="Masukkan Nama Lengkap Anda" name="nama" required>
                          <label for="nomor"><strong>Nomor Karyawan</strong></label>
                          <input type="text" placeholder="Masukkan Nomor Karyawan" name="nomor" required>
                          <label for="telp"><strong>Nomor Telephone</strong></label>
                          <input type="text" placeholder="Masukkan Nomor Telephone Anda" name="telp" required>
                          <label for="jabatan"><strong>Jabatan</strong></label>
                          <input type="text" placeholder="Masukkan Jabatan Anda" name="jabatan" required>
                          <label for="divisi"><strong>Divisi</strong></label>
                          <input type="text" placeholder="Masukkan Divisi Anda" name="divisi" required>
                        </div>
                        <button type="submit"><strong>SIMPAN</strong></button>
                      </form>
                </div>
			</div>
		</div>
	</div>


	

<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>