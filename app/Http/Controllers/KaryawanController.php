<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KaryawanController extends Controller
{
    public function index()
    {
        $karyawan = DB::table('karyawan')->get();
        return view('index',['karyawan'=> $karyawan]);
    }
    public function adddata()
    {
        return view('adddata');
    }
    public function store(Request $request)
    {
        DB::table ('karyawan')->insert([
            'nama_karyawan' => $request->nama,
            'no_karyawan' => $request->nomor,
            'no_telp_karyawan' => $request->notlp,
            'id' => $request->id,
            'jabatan_karyawan' => $request->jabatan,
            'divisi_karyawan' => $request->divisi
        ]);
        return redirect('/');
    }
    public function edit($id)
    {
        $karyawan = DB::table('karyawan')->where('id',$id)->get();
        return view('edit',['karyawan'=> $karyawan]);
    }
    public function update(Request $request)
    {
        DB::table('karyawan')->where('id',$request->id)->update([
            'nama_karyawan' => $request->nama,
            'no_karyawan' => $request->nomor,
            'no_telp_karyawan' => $request->notlp,
            'jabatan_karyawan' => $request->jabatan,
            'divisi_karyawan' => $request->divisi
        ]);
        return redirect('/');
    }
    public function hapus($id)
    {
        DB::table('karyawan')->where('id',$id)->delete();
        return redirect('/');
    }
}
